<?php

namespace Drupal\fillpdf_uuid\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\fillpdf\FillPdfContextManagerInterface;
use Drupal\fillpdf\Service\FillPdfContextManager;

/**
 * Helper class to load entities from a FillPDF context array.
 */
class FillPdfUuidContextManager extends FillPdfContextManager
{

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(FillPdfContextManagerInterface $fillpdf_context_manager, EntityTypeManagerInterface $entity_type_manager)
  {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($this->entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntities(array $context)
  {
    $entities = [];

    foreach ($context['entity_ids'] as $entity_type => $entity_ids) {
      $type_controller = $this->entityTypeManager->getStorage($entity_type);
      $config = \Drupal::service('config.factory')->getEditable('fillpdf.settings');
      ;
      if ($config->get('enable_uuid')) {
        $storage = $this->entityTypeManager->getStorage($entity_type)->getQuery();
        $entity_ids = $storage->condition('uuid', $entity_ids, 'IN')->execute();
      }
      $entity_list = $type_controller->loadMultiple($entity_ids);
      if (!empty($entity_list)) {
        // Initialize array.
        $entities += [$entity_type => []];
        $entities[$entity_type] += $entity_list;
      }
    }

    return $entities;
  }
}
